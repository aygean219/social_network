import domain.Friendship;
import domain.Tuple;
import domain.User;
import repository.Repository;
import repository.database.FriendshipDatabaseRepository;
import repository.database.UserDatabaseRepository;
import service.FriendshipService;
import service.NetworkService;
import service.UserService;
import userinterface.UserInterface;
import validators.FriendshipValidator;
import validators.UserValidator;
import validators.ValidationException;
import validators.Validator;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException {

        /*
        String fileNameUser = "data/users.csv";
        String fileNameFriendship = "data/friendships.csv";

        
        Repository<Long, User> userFileRepository = new UserFile(fileNameUser, new UserValidator());
        Repository<Tuple<Long,Long>, Friendship> friendshipFileRepository = new FriendshipFile(fileNameFriendship,new FriendshipValidator());
        
        UserService serviceUser = new UserService(userFileRepository);
        FriendshipService serviceFriendship = new FriendshipService(friendshipFileRepository,userFileRepository);
        
        UserInterface userInterface = new UserInterface(serviceUser,serviceFriendship);
        userInterface.show();
        

        Repository<Long,User> userRepository = new InMemoryRepository<>( new UserValidator());
        Repository<Tuple<Long,Long>, Friendship> friendshipRepository = new InMemoryRepository<>(new FriendshipValidator());
        UserService userService = new UserService(userRepository);
        FriendshipService friendshipService = new FriendshipService(friendshipRepository,userRepository);

        UserInterface userInterface = new UserInterface(userService,friendshipService);
        userInterface.show();
        */
        Repository<Long,User> userDatabaseRepository = new UserDatabaseRepository("jdbc:postgresql://localhost:5432/lab3_database","postgres","aygean");
        Repository<Tuple<User,User>, Friendship> friendshipDatabaseRepository = new FriendshipDatabaseRepository("jdbc:postgresql://localhost:5432/lab3_database","postgres","aygean");

        UserService userService = new UserService(userDatabaseRepository,new UserValidator());
        FriendshipService friendshipService = new FriendshipService(friendshipDatabaseRepository,new FriendshipValidator());
        NetworkService networkService = new NetworkService(friendshipDatabaseRepository,userDatabaseRepository);
        UserInterface userInterface = new UserInterface(userService,friendshipService,networkService);
        userInterface.show();

    }
}
