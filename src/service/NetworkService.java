package service;

import domain.Friendship;
import domain.Tuple;
import domain.User;
import repository.Repository;
import repository.memory.RepositoryException;
import utils.Graph;
import validators.FriendshipValidator;
import validators.Validator;

import java.util.ArrayList;

public class NetworkService {
    private final Repository<Tuple<User,User>, Friendship> repo;
    private final Repository<Long, User> repoUser;

    public NetworkService(Repository<Tuple<User,User>, Friendship> repo, Repository<Long, User> repoUser) {
        this.repo = repo;
        this.repoUser = repoUser;
    }

    /**
     * @return the community number
     * */
    public int numberCommunities(){
        int nod=0;
        for(User x: repoUser.findAll()){
            if(nod < x.getId().intValue()){
                nod = x.getId().intValue();
            }
        }
        nod++;
        Graph graph = new Graph(nod);
        repo.findAll().forEach(x->graph.addEdge(x.getId().getE1().getId().intValue(),x.getId().getE2().getId().intValue()));
        return graph.connectedComponents();
    }

    /**
     * @return an array of integers with the id of the community users
     * */
    public ArrayList<Integer> longestCommunity(){
        int nod = 0;
        for( User x: repoUser.findAll()) {
            if (nod < x.getId().intValue()) {
                nod = x.getId().intValue();
            }
        }
        nod++;
        Graph graph = new Graph(nod);
        repo.findAll().forEach(x->graph.addEdge(x.getId().getE1().getId().intValue(),x.getId().getE2().getId().intValue()));
        return graph.longestPath();

    }
    public void addFriendship(String id1,String id2){
        Long first = FriendshipValidator.validateId(id1);
        Long second = FriendshipValidator.validateId(id2);
        if(first>second){
            Long aux= first;
            first = second;
            second = aux;
        }
        User user1 = repoUser.findOne(first).get();
        User user2 = repoUser.findOne(second).get();
        repo.save(new Friendship(new Tuple<User,User>(user1,user2),""));
    }

    public void deleteFriendship(String id1,String id2){
        Long first = FriendshipValidator.validateId(id1);
        Long second = FriendshipValidator.validateId(id2);
        if(first>second){
            Long aux= first;
            first = second;
            second = aux;
        }
        User user1 = repoUser.findOne(first).get();
        User user2 = repoUser.findOne(second).get();
        repo.delete(new Tuple<User,User>(user1,user2));
    }
    public void deleteMoreFriendships(String id){
        ArrayList<Friendship> friendships = repo.findAll();
        for(Friendship f: friendships){
            String left = f.getId().getE1().toString();
            String right = f.getId().getE2().toString();

            if(left.equals(id) || right.equals(id)){
                try{
                    deleteFriendship(left,right);
                }catch (RepositoryException ignored){

                }
            }
        }
    }
}
