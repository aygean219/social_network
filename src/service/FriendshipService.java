package service;

import domain.Friendship;
import domain.Tuple;
import domain.User;
import repository.Repository;
import repository.memory.RepositoryException;
import utils.Graph;
import validators.FriendshipValidator;
import validators.Validator;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class FriendshipService {
    private final Repository<Tuple<User,User>, Friendship> repo;
    private Validator<Friendship> validator;

    public FriendshipService(Repository<Tuple<User, User>, Friendship> repo,Validator<Friendship> validator) {
        this.repo = repo;
        this.validator = validator;
    }

    /**
     * @return all the friendhips
     * */
    public ArrayList<Friendship> getAll(){
        return repo.findAll();
    }

    /**
     * Add a friendship with the specified id
     * @param id1 - id of the first user
     * @param id2 - id of the second user
     */
    public void addFriendhip(Friendship friendship){
        validator.validate(friendship);
        repo.save(friendship);
    }

    /**
     * Delete a friendship that have the specified ids
     * @param id1 - id of the first user
     * @param id2 - id of the second user
     */
    public void deleteFriendShip(Tuple<User,User> tuple){
        repo.delete(tuple);
    }

    /**
     * Delete all the friendships that have an id equal to that given
     * @param id - The id to be deleted
     */



    /**
     * @return the community number
     * */
    /*public int numberCommunities(){
        int nod=0;
        for(User x: repoUser.findAll()){
            if(nod < x.getId().intValue()){
                nod = x.getId().intValue();
            }
        }
        nod++;
        Graph graph = new Graph(nod);
        repo.findAll().forEach(x->graph.addEdge(x.getId().getE1().intValue(),x.getId().getE2().intValue()));
        return graph.connectedComponents();
    }
    */


    /**
     * @return an array of integers with the id of the community users
     * */
    /*
    public ArrayList<Integer> longestCommunity(){
        int nod = 0;
        for( User x: repoUser.findAll()) {
            if (nod < x.getId().intValue()) {
                nod = x.getId().intValue();
            }
        }
        nod++;
        Graph graph = new Graph(nod);
        repo.findAll().forEach(x->graph.addEdge(x.getId().getE1().intValue(),x.getId().getE2().intValue()));
        return graph.longestPath();

    }*/
}
