package repository.database;

import domain.User;
import repository.Repository;
import repository.memory.RepositoryException;

import java.sql.*;
import java.util.ArrayList;
import java.util.Optional;

public class UserDatabaseRepository implements Repository<Long, User> {
    private final String url;
    private final String username;
    private final String password;

    public UserDatabaseRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public Optional<User> findOne(Long id) {
    String querry = "SELECT * FROM userr WHERE id="+id+";";
        try (
    Connection connection = DriverManager.getConnection(url, username, password);
    PreparedStatement statement = connection.prepareStatement(querry);
    ResultSet resultSet = statement.executeQuery()) {

        Optional<User> f = null;
        while(resultSet.next()) {
            Long id1 = Long.parseLong(resultSet.getString(1));
            String first_name = resultSet.getString(2);
            String last_name = resultSet.getString(3);
            String date = resultSet.getString(4);
            String gender = resultSet.getString(5);
            User user = new User(first_name, last_name, date, gender);
            user.setId(id1);
            f = Optional.of(user);
        }
        return f;
    } catch (SQLException e) {
        e.printStackTrace();
    }
        return Optional.empty();
    }

    @Override
    public ArrayList<User> findAll() {
        ArrayList<User> users = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from userr");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("firstName");
                String lastName = resultSet.getString("lastName");
                String date = resultSet.getString("date");
                String gender = resultSet.getString("gender");

                User user = new User(firstName, lastName, date, gender);
                user.setId(id);
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public Optional<User> save(User entity) {
        //validator.validate(entity);
        String querry = "INSERT INTO userr VALUES('"+entity.getId().intValue()+"','"
                +entity.getFirstName()+"','"+entity.getLastName()+"','"
                +entity.getDate()+"','"+entity.getGender()+"')";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(querry);
        )
        {
            statement.execute();
            return Optional.of(entity);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> delete(Long id) {
        Optional<User> aux = findOne(id);
        if(aux == null){
            throw new RepositoryException("User does not exist!");
        }
        String querry = "DELETE FROM userr WHERE id="+aux.get().getId()+";";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(querry);
        )
        {
            statement.execute();
            return Optional.empty();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return aux;
    }

    @Override
    public Optional<User> update(User entity) {
        String sql = "update userr set first_name=?, last_name=?, birth_date=? where id=?";

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {

            ps.setString(1, entity.getFirstName());
            ps.setString(2, entity.getLastName());
            ps.setString(3, entity.getDate());
            ps.setLong(4, entity.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}


