package userinterface;

import domain.Friendship;
import domain.User;
import repository.memory.RepositoryException;
import service.FriendshipService;
import service.NetworkService;
import service.UserService;
import validators.ValidationException;

import java.util.ArrayList;
import java.util.Scanner;

public class UserInterface {
    private final UserService userService;
    private final FriendshipService friendshipService;
    private final NetworkService networkService;
    private final Scanner in = new Scanner(System.in);

    public UserInterface(UserService userService, FriendshipService friendshipService,NetworkService networkService) {
        this.userService = userService;
        this.friendshipService = friendshipService;
        this.networkService = networkService;
    }

    /**
     * Print the menu
     * */

    private void menu(){
        System.out.println("--------------------------MENU--------------------------");
        System.out.println("|   1. Add user                                        |");
        System.out.println("|   2. Delete user                                     |");
        System.out.println("|   3. Show users                                      |");
        System.out.println("|   4. Add friendship                                  |");
        System.out.println("|   5. Delete friendship                               |");
        System.out.println("|   6. Show friendships                                |");
        System.out.println("|   7. Number of communities                           |");
        System.out.println("|   8. The most sociable community                     |");
        System.out.println("|   0. Exit                                            |");
        System.out.println("--------------------------------------------------------");
        System.out.println();
        System.out.println("Insert the number option: ");
    }

    private void printException(String msg){
        String[] tokens = msg.split("/");
        tokens[0] = tokens[0].split(":")[1];
        for(String t: tokens){
            System.out.println(t);
        }
    }

    /**
     * Print all the users
     * */
    private void showUser(){
        ArrayList<User> users = userService.getAll();
        for(User user : users){
            System.out.println("id=" + user.getId() + ", "+user.toString());
        }
    }

    /**
     * Add an user
     * */
    private void addUser(){
        String firstName,lastName,date,gender;

        System.out.println("First name:");
        firstName = in.nextLine();

        System.out.println("Last name:");
        lastName = in.nextLine();

        System.out.println("Birthday (yyyy-mm-dd): ");
        date = in.nextLine();

        System.out.println("Which gender: male->M ,female->F,none->N");
        gender = in.nextLine();

        try{
            userService.addUser(firstName,lastName,date,gender);
            System.out.println();
            System.out.println(firstName + " saved successfully!");
        }catch (ValidationException | RepositoryException e){
            printException(e.toString());
        }
    }

    /**
     * Delete a user
     * */
    private void deleteUser(){
        showUser();
        System.out.println();

        String id1;
        System.out.println("Insert the id of User you would like to delete: ");
        id1=in.nextLine();

        try{
            userService.deleteUser(id1);
            System.out.println("User with id = " + id1 + " successfully deleted!");
            networkService.deleteMoreFriendships(id1);
        }catch (RepositoryException e){
            printException(e.toString());
        }
    }

    /**
     * Print all the friendships
     * */

    private void showFriendships(){
        ArrayList<Friendship> friendships = friendshipService.getAll();
        for(Friendship f: friendships){
            System.out.println(f.toString());
        }
    }

    /**
     * Add a friendship with the ids provided by the user
     * */
    private void addFriendship(){
        showUser();
        System.out.println();

        String id1,id2;
        System.out.println("Insert first id: ");
        id1=in.nextLine();

        System.out.println("Insert second id: ");
        id2=in.nextLine();

        try {
            networkService.addFriendship(id1,id2);
            System.out.println("Friendship saved successfully!");
        }catch (ValidationException | RepositoryException e){
            printException(e.toString());
        }
    }

    /**
     * Delete a friendship with the ids provided by the user
     * */
    private void deleteFriendship(){
        showFriendships();
        System.out.println();

        String id1,id2;
        System.out.println("Insert first id: ");
        id1=in.nextLine();
        System.out.println("Insert second id: ");
        id2= in.nextLine();
        try {
            networkService.deleteFriendship(id1,id2);
            System.out.println("Friendship successfully deleted!");
        }catch (RepositoryException | ValidationException e){
            printException(e.toString());
        }
    }

    /**
     * Print the number of users and the users of the most sociable community
     */
    private void longestCommunities(){
        ArrayList<Integer> result= new ArrayList<>(networkService.longestCommunity());
        System.out.println("The number of users is: " + result.size());
        System.out.println("Users are: " + result.toString());
    }

    /**
     * The show function handles the menu presentation
     */
    public void show(){
        String request;
        while(true) {
            menu();
            request = in.nextLine();
            System.out.println();
            switch (request) {
                case "1":
                    addUser();
                    System.out.println();
                    break;
                case "2":
                    deleteUser();
                    System.out.println();
                    break;
                case "3":
                    showUser();
                    System.out.println();
                    break;
                case "4":
                    addFriendship();
                    System.out.println();
                    break;
                case "5":
                    deleteFriendship();
                    System.out.println();
                    break;
                case "6":
                    showFriendships();
                    System.out.println();
                    break;
                case "7":
                    System.out.println("The number of communities is: " + networkService.numberCommunities());
                    System.out.println();
                    break;
                case "8":
                    longestCommunities();
                    System.out.println();
                    break;
                case "0":
                    System.out.println("Exit!");
                    System.exit(0);
                default:
                    System.out.println("The operation is invalid!");
                    System.out.println();
            }
        }
    }
}
